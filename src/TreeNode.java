public class TreeNode {
    int value;
    TreeNode left;
    TreeNode right;

    public TreeNode(int value) {
        this.value = value;
        left = null;
        right = null;
    }

    public class BinaryTree {
        private TreeNode root;

        public BinaryTree() {
            root = null;
        }

        public void insert(int value) {
            root = insertRec(root, value);
        }

        private TreeNode insertRec(TreeNode root, int value) {
            if (root == null) {
                root = new TreeNode(value);
                return root;
            }

            if (value < root.value) {
                root.left = insertRec(root.left, value);
            } else if (value > root.value) {
                root.right = insertRec(root.right, value);
            }

            return root;
        }

        public void inOrderTraversal(TreeNode root) {
            if (root != null) {
                inOrderTraversal(root.left);
                System.out.print(root.value + " ");
                inOrderTraversal(root.right);
            }
        }
    }

}
